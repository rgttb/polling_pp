import os
import sys
import json
import base64
import requests
from constantfile import *


print ("Scanning bit bucket for pull requests...")

# get pull requests
pull_requests_info = requests.request("GET", auth=(git_username, git_password), url=pull_requests_url)

merge_request_details_list = []
if pull_requests_info.ok:
    for pull_requests in pull_requests_info.json()['values']:
        if pull_requests['destination']['branch']['name'].lower() == branch_to_watch:
            source_branch = pull_requests['source']['branch']['name'].lower()
            merge_request_details_list.append({'merge_request_id': pull_requests['id'], 'source_branch': source_branch})
else:
    print ("Failed to scan bit bucket. Response from API - " + str(pull_requests_info.text))
    raise Exception("Failed to scan bitbucket " + str(pull_requests_info.text))

print ("Bit bucket scanning process ended successfully!")

if len(merge_request_details_list) == 0:
    print ("No open merge requests detected!")
    sys.exit(0)

try:
    merge_request_details = json.loads(open('./unit_testing_log.json', 'r').read())



    crumb_response = requests.request("GET", crumb_issue_url, auth=(jenkins_username, jenkins_password))
    if not crumb_response.ok:
        raise Exception("Failed to get crumb token. ERROR - " + str(crumb_response.text))
    if 'crumb' not in crumb_response.json().keys():
        raise Exception("Crumb token not available in response!")
    crumb_token = crumb_response.json()['crumb']
    print ("Crumb token found - {crumb}".format(crumb=crumb_token))
    for merge_request_item in merge_request_details_list:
        print("---------------------------------------------------------")
        print(merge_request_item["merge_request_id"])
        print(merge_request_details)
        for each_merge in merge_request_details:
            print("*************************************")
            print(merge_request_details)
            print(each_merge["merge_request_id"])
            if str(merge_request_item["merge_request_id"]) != str(each_merge["merge_request_id"]):
                project_build_params = {"MERGE_REQUEST_ID": merge_request_item['merge_request_id'],
                                        "SOURCE_BRANCH": merge_request_item['source_branch'], "delay": "0sec"}
                body = {"crumb": crumb_token}
                headers = {"Jenkins-Crumb": crumb_token}
                response = requests.request("POST", trigger_job_url, params=project_build_params,
                                            auth=(jenkins_username, jenkins_password), data=body, headers=headers)
                if response.ok:
                    print(
                        "ScanChanges triggered successfully for merge request id {id}, source branch is {branch}".format(
                            id=merge_request_item, branch=merge_request_item['source_branch']))
                else:
                    raise Exception("Failed to trigger ScanChanges for merge request id {id}\nERROR - {error}".format(
                        id=merge_request_item, error=response.text))
                break
            else:
                print ("Pipeline has already been triggered")
                pass

except Exception as e:
    print("Unable to open the file  - " + str(e))
